﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

namespace FileTransfer
{
    class Client
    {
        static void Main(string[] args)
        {
            try
            {
                string startPath = @"";
                string zipPath = @"";

                string startFilePath = zipPath;
                string endFilePath = @""; 
                string sendFile = endFilePath;

                Random rnd = new Random();
                int pass = rnd.Next();
                string secretKey = pass.ToString();

                EncryptFile(startFilePath, endFilePath, secretKey);
                Console.WriteLine("This file encrypted ...........   pass:..... " + secretKey + "\n");

                Console.WriteLine("File from this .......... " + endFilePath);

                TcpClient tcpClient = new TcpClient("127.0.0.1", 8888);
                Console.WriteLine("Connect .....  .... send ..............");
                StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                ZipFile.CreateFromDirectory(startPath, zipPath);


                sWriter.WriteLine(secretKey);
                sWriter.WriteLine("");
                byte[] bytes = File.ReadAllBytes(sendFile);
                sWriter.WriteLine(bytes.Length.ToString());
                sWriter.Flush();
                sWriter.WriteLine(sendFile);
                sWriter.Flush();

                Console.WriteLine("This file compressed...............");

                Console.WriteLine("Sending......");
                tcpClient.Client.SendFile(sendFile);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            Console.Read();
        }

        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 512;
                    AES.BlockSize = 256;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 16);
                    AES.IV = key.GetBytes(AES.BlockSize / 16);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        public static void EncryptFile(string file, string fileEncrypted, string password)
        {
            byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);
            File.WriteAllBytes(fileEncrypted, bytesEncrypted);
            return null;
        }
    }
}



//Percobaan 2 .... if semua kali 02