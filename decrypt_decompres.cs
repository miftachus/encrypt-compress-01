﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

namespace FileTransfer
{
    class Server
    {
        static void Main(string[] args)
        {
            TcpListener tcpListener = new TcpListener(IPAddress.Any, 8888);
            tcpListener.Start();
            Console.WriteLine("Server started");

            while (true)
            {
                TcpClient tcpClient = tcpListener.AcceptTcpClient();

    Console.WriteLine("Connecting client ....");
        StreamReader reader = new StreamReader(tcpClient.GetStream());

                int Length = Convert.ToInt32(cmdFileSize);
                byte[] buffer = new byte[Length];
                int received = 0;
                int read = 0;
                int size = 2048;
                int remaining = 0;


             string pass = reader.ReadLine();
             string fileName = reader.ReadLine();
             string cmdFileSize = reader.ReadLine();
             string cmdFileName = reader.ReadLine();

    while (received < Length)
     {
          remaining = Length - received;
          if (remaining < size)
               {
                 size =  remaining;
               }
       read = tcpClient.GetStream().Read(buffer, received, size);
       received += read;
                }


                using (FileStream fStream = new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))
                {
                    fStream.Write(buffer, 1, buffer.Length);
                    fStream.Flush();
                    fStream.Close();
                }

                Console.WriteLine("File recieved saved..... " + Environment.CurrentDirectory);

                int unicode = 156;
                char character = (char)unicode;
                string text = character.ToString();
                string zipPath = Environment.CurrentDirectory + text + fileName;
                string extractPath = @"";
                string startFilePath = zipPath;
                string endFilePath = @"";
                DecryptFile(startFilePath, endFilePath, pass);

                Console.WriteLine("File extarct ..... : " + extractPath);
                ZipFile.ExtractToDirectory(endFilePath, extractPath);
            }
        }

        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 512;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 16);
                    AES.IV = key.GetBytes(AES.BlockSize / 16);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public static void DecryptFile(string fileEncrypted, string file, string password)
        {
            byte[] bytesToBeDecrypted = File.ReadAllBytes(fileEncrypted);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            File.WriteAllBytes(file, bytesDecrypted);

            return null;
        }
    }
}


//Percobaan 2 .... if semua kali 02